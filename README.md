**How to build (example: version 6.4.3-1):**

online / master branch

```
cd docker/from-packages
docker build --no-cache --build-arg version=6.4 --build-arg type=key -t registry.gitlab.collabora.com/collabora-online/docker -f Ubuntu .
docker tag registry.gitlab.collabora.com/collabora-online/docker:latest registry.gitlab.collabora.com/collabora-online/docker:6.4.3.1
docker push registry.gitlab.collabora.com/collabora-online/docker
```